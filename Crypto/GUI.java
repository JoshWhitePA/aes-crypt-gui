
package Crypto;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.*;

import Crypto.Encryption.InvalidAESStreamException;
import Crypto.Encryption.InvalidKeyLengthException;
import Crypto.Encryption.InvalidPasswordException;
import Crypto.Encryption.StrongEncryptionNotAvailableException;

import java.awt.*;
import java.applet.Applet;

public class GUI extends JFrame implements WindowListener, ActionListener{
	JButton btnEncry,
			btnDencry,
			btnOpenFile;
	JPanel panel;
	JButton button;
	JPasswordField txtPassOne,
				   txtPassTwo;
	JLabel lblPassOne,
		   lblPassTwo,
		   heading;
	JFileChooser fileChooser;
	Encryption encry;
	
	  public GUI() {
		   super("File Encrypter");
		   encry = new Encryption();
		   
		   panel = new JPanel();//creates the jpanel
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setLayout(null);//told not to use a layout manager
			setSize((int)getScreenSize().getWidth(),(int)getScreenSize().getHeight());//JFrame size 
			setResizable(false);//user cannot resize the window
			panel.setBounds(0,0,1000,700);//creates jpanel bounds
			addWindowListener(this);
			
			
			heading = new JLabel("Choose what action you would like to perform.");
			Font headingFont = new Font("Serif", Font.BOLD, 30);
			heading.setBounds(300,10,700,80);
			heading.setFont(headingFont);
			add(heading);

			 
			btnEncry = new JButton("Encrypt");
			btnDencry = new JButton("Dencrypt");
			btnOpenFile = new JButton("Open File");
			
			btnEncry.setBounds( ((int)(getScreenSize().getWidth()/2)-(20)),(int)(getScreenSize().getHeight()/3.5),170,40);
			btnEncry.setEnabled(true);
			add(btnEncry);
			btnEncry.addActionListener(this);
			
		
			btnDencry.setBounds((int)(getScreenSize().getWidth()/2)-(20),(int)(getScreenSize().getHeight()/2),170,40);
			btnDencry.setEnabled(true);
			add(btnDencry);
			btnDencry.addActionListener(this);
			
			btnOpenFile.setBounds((int)(getScreenSize().getWidth()/2)-(20),(int)(getScreenSize().getHeight()/2),170,40);
			btnOpenFile.setEnabled(true);
			btnOpenFile.setVisible(false);
			add(btnOpenFile);
			btnOpenFile.addActionListener(this);
			
			Font labelFonts = new Font("Serif", Font.PLAIN, 14);
			
			lblPassOne = new JLabel("Enter Password used for encryption");
			lblPassOne.setBounds( ((int)getScreenSize().getWidth()/2)-(20),(int)(getScreenSize().getHeight()/2)-(120),700,80);
			lblPassOne.setFont(labelFonts);
			lblPassOne.setVisible(false);
			add(lblPassOne);
			
			txtPassOne = new JPasswordField(1);
			txtPassOne.setBounds((int)(getScreenSize().getWidth()/2)-(20),(int)(getScreenSize().getHeight()/2)-(70), 170, 30);
			txtPassOne.setEnabled(true);
			txtPassOne.setVisible(false);
			add(txtPassOne);
			
			lblPassTwo = new JLabel("Re-enter Password used for encryption");
			lblPassTwo.setBounds( ((int)getScreenSize().getWidth()/2)-(20),(int)(getScreenSize().getHeight()/2)-(75),700,80);
			lblPassTwo.setFont(labelFonts);
			lblPassTwo.setVisible(false);
			add(lblPassTwo);
			
			txtPassTwo = new JPasswordField(1);
			txtPassTwo.setBounds((int)(getScreenSize().getWidth()/2)-(20),(int)(getScreenSize().getHeight()/2)-(30), 170, 30);
			txtPassTwo.setEnabled(true);
			txtPassTwo.setVisible(false);
			add(txtPassTwo);
			
			/*fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));*/
/*			 fileChooser.setDialogTitle("Choose a file");
			 this.getContentPane().add(fileChooser);
			 fileChooser.setVisible(true);
			*/
			//adds the jpanel to the jframe
			add(panel);
			setVisible(true);
	    }
	  
	  protected File[] openAction()
	  {
	      FileDialog dialog;
	      File[] file = null;

	      String mHomeDir = System.getProperty("user.home");
	      dialog = new FileDialog (this);
	      dialog.setMultipleMode(true);
	      dialog.setMode (FileDialog.LOAD);
	      dialog.setTitle ("Open");
	      dialog.setDirectory (mHomeDir);
	      dialog.setVisible (true);
	      if (null != dialog.getFiles()){
	          mHomeDir = dialog.getDirectory ();
	          file = dialog.getFiles ();
	          for(int idx = 0; idx < file.length;idx++){
	        	  System.out.println(file[idx].getAbsolutePath ());
	          }
	      }
	      return file;
	  }
	  private Dimension getScreenSize(){
		  Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		  double width,height;
		  height = screenSize.getHeight() - (screenSize.getHeight()/4);
		  width = screenSize.getWidth() - (screenSize.getWidth()/4);
		  screenSize.setSize(width,height);
		  return screenSize;
	  }
	  
	  private void setEncryptControlsVis(boolean isShown){
		    btnEncry.setVisible(!isShown);
			btnDencry.setVisible(!isShown);
			txtPassOne.setVisible(isShown);
			txtPassTwo.setVisible(isShown);
			lblPassOne.setVisible(isShown);
			lblPassTwo.setVisible(isShown);
			btnOpenFile.setVisible(isShown);
	  }
	  
	  private void setDencryptControlsVis(boolean isShown){
		    btnEncry.setVisible(!isShown);
			btnDencry.setVisible(!isShown);
			txtPassOne.setVisible(isShown);
			txtPassTwo.setVisible(!isShown);
			lblPassOne.setText("Enter password to dencrypt file(s)");
			lblPassTwo.setText("Re-Enter Password");
			lblPassOne.setVisible(isShown);
			lblPassTwo.setVisible(!isShown);
			btnOpenFile.setText("Open File(s)");
			btnOpenFile.setVisible(isShown);
	  }

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (((JButton)e.getSource()).getActionCommand() == "Encrypt" ){
			setEncryptControlsVis(true);
		}
		else if (((JButton)e.getSource()).getActionCommand() == "Dencrypt" ){
			setDencryptControlsVis(true);
		}
		else if (((JButton)e.getSource()).getActionCommand() == "Open File" ){
			char[] password = null;
			if (txtPassOne.getPassword() == txtPassTwo.getPassword()){
				password = (char[])txtPassOne.getPassword();
			}
			File[] files = openAction();
			InputStream fileStream;
			OutputStream output;
			for(int idx = 0; idx < files.length; idx++ ){
				try {
					fileStream = new FileInputStream(files[idx]);
					output = new FileOutputStream(files[idx]+".aes");
					encry.encrypt(128, password, fileStream, output);
					output.close();
				} catch (StrongEncryptionNotAvailableException | IOException | InvalidKeyLengthException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		else if (((JButton)e.getSource()).getActionCommand() == "Open File(s)" ){
			char[] password = null;
			if (txtPassOne.getPassword() != null){
				password = (char[])txtPassOne.getPassword();
			}
			File[] files = openAction();
			InputStream fileStream;
			OutputStream output;
			for(int idx = 0; idx < files.length; idx++ ){
				try {
					fileStream = new FileInputStream(files[idx]);
					output = new FileOutputStream(files[idx]+".png");
					encry.decrypt(password, fileStream, output);
					output.close();
				} catch (StrongEncryptionNotAvailableException | IOException | InvalidPasswordException | InvalidAESStreamException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}
